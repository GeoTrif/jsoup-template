package com.template.model;

import java.io.Serializable;

public class CompanyInfo implements Serializable {

    private String name;
    private String location;
    private String numberOfEmployees;
    private String industry;
    private String description;

    public CompanyInfo() {
    }

    public CompanyInfo(String name, String location, String numberOfEmployees, String industry, String description) {
        this.name = name;
        this.location = location;
        this.numberOfEmployees = numberOfEmployees;
        this.industry = industry;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getNumberOfEmployees() {
        return numberOfEmployees;
    }

    public void setNumberOfEmployees(String numberOfEmployees) {
        this.numberOfEmployees = numberOfEmployees;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "CompanyInfo{" +
                "name='" + name + '\'' +
                ", location='" + location + '\'' +
                ", numberOfEmployees='" + numberOfEmployees + '\'' +
                ", industry='" + industry + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
