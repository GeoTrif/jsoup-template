package com.template.model;

import java.io.Serializable;

public class EmployeeInfo implements Serializable {

    private String name;
    private String occupation;
    private String industry;
    private String phoneNumber;
    private String address;

    public EmployeeInfo() {
    }

    public EmployeeInfo(String name, String occupation, String industry, String phoneNumber, String address) {
        this.name = name;
        this.occupation = occupation;
        this.industry = industry;
        this.phoneNumber = phoneNumber;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "EmployeeInfo{" +
                "name='" + name + '\'' +
                ", occupation='" + occupation + '\'' +
                ", industry='" + industry + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
