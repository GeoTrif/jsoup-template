package com.template.restcontroller;

import com.template.model.CompanyInfo;
import com.template.model.EmployeeInfo;
import com.template.service.JsoupTemplateService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class JsonTemplaterRestController {

    private static final String JSOUP_TEMPLATE_REST_API_ENDPOINT = "/get-infos";

    private final JsoupTemplateService jsoupTemplateService;

    public JsonTemplaterRestController(JsoupTemplateService jsoupTemplateService) {
        this.jsoupTemplateService = jsoupTemplateService;
    }

    @GetMapping(JSOUP_TEMPLATE_REST_API_ENDPOINT)
    public Map<EmployeeInfo, CompanyInfo> getInfoAboutEmployeeAndCompany() {
        return jsoupTemplateService.getDemoDetails();
    }
}
