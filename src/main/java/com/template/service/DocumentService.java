package com.template.service;

import org.jsoup.nodes.Document;
import org.springframework.stereotype.Service;

@Service
public class DocumentService {

    private static final String EMPLOYEE_ADDRESS_ID = "address";
    private static final String COMPANY_LOCATION_ID = "location";
    private static final String COMPANY_ADDRESS_ID = "company-name";
    private static final String COMPANY_DESCRIPTION_ID = "description";
    private static final String COMPANY_INDUSTRY_ID = "company-industry";
    private static final String EMPLOYEE_NAME_SELECTOR = "#employee-name";
    private static final String EMPLOYEE_PHONE_NUMBER_ID = "phone-number";
    private static final String EMPLOYEE_INDUSTRY_ID = "employee-industry";
    private static final String EMPLOYEE_OCCUPATION_SELECTOR = "#occupation";
    private static final String COMPANY_NUMBER_OF_EMPLOYEES_ID = "number-of-employees";

    String getEmployeeName(final Document document) {
        return document.select(EMPLOYEE_NAME_SELECTOR).text();
    }

    String getEmployeeOccupation(final Document document) {
        return document.select(EMPLOYEE_OCCUPATION_SELECTOR).text();
    }

    String getEmployeeIndustry(final Document document) {
        return document.getElementById(EMPLOYEE_INDUSTRY_ID).text();
    }

    String getEmployeePhoneNumber(final Document document) {
        return document.getElementById(EMPLOYEE_PHONE_NUMBER_ID).text();
    }

    String getEmployeeAddress(final Document document) {
        return document.getElementById(EMPLOYEE_ADDRESS_ID).text();
    }

    String getCompanyName(final Document document) {
        return document.getElementById(COMPANY_ADDRESS_ID).text();
    }

    String getCompanyLocation(final Document document) {
        return document.getElementById(COMPANY_LOCATION_ID).text();
    }

    String getCompanyNumberOfEmployees(final Document document) {
        return document.getElementById(COMPANY_NUMBER_OF_EMPLOYEES_ID).text();
    }

    String getCompanyIndustry(final Document document) {
        return document.getElementById(COMPANY_INDUSTRY_ID).text();
    }

    String getCompanyDescription(final Document document) {
        return document.getElementById(COMPANY_DESCRIPTION_ID).text();
    }
}
