package com.template.service;

import com.template.model.CompanyInfo;
import com.template.model.EmployeeInfo;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Service
public class JsoupTemplateService {

    private static final String CHARSET_FORMAT = "UTF-8";
    private static final String JSOUP_PARSE_DEMO_PATHNAME = "jsoup-parse-demo.html";

    private final DocumentService documentService;

    public JsoupTemplateService(DocumentService documentService) {
        this.documentService = documentService;
    }

    public Map<EmployeeInfo, CompanyInfo> getDemoDetails() {
        Map<EmployeeInfo, CompanyInfo> mapInfo = new HashMap<>();

        try {
            // If you want to connect to a web page(ex: Facebook, LinkedIn, etc), use the line below:
            // Document document = Jsoup.connect("https://www.apollo.io/people/Geo/Trif/57d6dd5ca6da985449282a31").get();
            Document document = Jsoup.parse(new File(JSOUP_PARSE_DEMO_PATHNAME), CHARSET_FORMAT);

            mapInfo.put(getEmployeeInfo(document), getCompanyInfo(document));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return mapInfo;
    }

    private EmployeeInfo getEmployeeInfo(final Document document) {
        EmployeeInfo employeeInfo = new EmployeeInfo();
        employeeInfo.setName(documentService.getEmployeeName(document));
        employeeInfo.setOccupation(documentService.getEmployeeOccupation(document));
        employeeInfo.setIndustry(documentService.getEmployeeIndustry(document));
        employeeInfo.setPhoneNumber(documentService.getEmployeePhoneNumber(document));
        employeeInfo.setAddress(documentService.getEmployeeAddress(document));

        return employeeInfo;
    }

    private CompanyInfo getCompanyInfo(final Document document) {
        CompanyInfo companyInfo = new CompanyInfo();
        companyInfo.setName(documentService.getCompanyName(document));
        companyInfo.setLocation(documentService.getCompanyLocation(document));
        companyInfo.setNumberOfEmployees(documentService.getCompanyNumberOfEmployees(document));
        companyInfo.setIndustry(documentService.getCompanyIndustry(document));
        companyInfo.setDescription(documentService.getCompanyDescription(document));

        return companyInfo;
    }
}
