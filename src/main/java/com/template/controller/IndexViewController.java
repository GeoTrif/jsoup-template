package com.template.controller;

import com.template.model.CompanyInfo;
import com.template.model.EmployeeInfo;
import com.template.service.JsoupTemplateService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.ArrayList;
import java.util.Map;

@Controller
public class IndexViewController {

    private static final String INDEX_PAGE_ENDPOINT = "/";
    private static final String INDEX_VIEW_NAME = "index";
    private static final String COMPANY_ATTRIBUTE_TAG_NAME = "company";
    private static final String EMPLOYEE_ATTRIBUTE_TAG_NAME = "employee";

    private final JsoupTemplateService jsoupTemplateService;

    public IndexViewController(JsoupTemplateService jsoupTemplateService) {
        this.jsoupTemplateService = jsoupTemplateService;
    }

    @GetMapping(INDEX_PAGE_ENDPOINT)
    public String getApolloInfo(Model model) {
        Map<EmployeeInfo, CompanyInfo> infoMap = jsoupTemplateService.getDemoDetails();

        model.addAttribute(EMPLOYEE_ATTRIBUTE_TAG_NAME, new ArrayList<>(infoMap.keySet()).get(0));
        model.addAttribute(COMPANY_ATTRIBUTE_TAG_NAME, infoMap.get(new ArrayList<>(infoMap.keySet()).get(0)));

        return INDEX_VIEW_NAME;
    }
}
